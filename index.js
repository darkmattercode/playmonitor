'use strict';

const
  express = require('express'),
  app = express(),
  mongoose = require('mongoose'),
  logger = require('morgan'),
  path = require('path'),
  passport = require('passport'),
  LocalStrategy = require('passport-local').Strategy,
  bodyParser = require('body-parser'),
  cookieParser = require('cookie-parser'),
  Tasks = require('./components/tasks');


app.use(logger('dev'));
app.set('views', __dirname + '/views');
app.set('view engine', 'jade');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: false,
    saveUninitialized: false
}));
app.use(express.static(path.join(__dirname, 'public')));
app.use(passport.initialize());
app.use(passport.session());
app.use(function (req, res, next) {
    res.locals.user = req.user;
    next();
});
app.listen(3030);

app.locals.pretty = true;

var models = require('./models');
var user = models.user;
passport.use(new LocalStrategy(user.authenticate()));
passport.serializeUser(user.serializeUser());
passport.deserializeUser(user.deserializeUser());

var minute = 1 * 60 * 1000;
var hour = minute * 60 * 60 * 1000;

setInterval(function() {Tasks.globalServerCheck()}, minute);

// mongoose
mongoose.Promise = Promise;
mongoose.connect('mongodb://localhost/game-monitor');

require('./router')(app);

module.exports = app;
