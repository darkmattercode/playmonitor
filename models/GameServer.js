const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema

var GameServer = new Schema({
  sid: Number,
  game: String,
  ip: String,
  port: Number,
  qport: Number,
  active: {
    type: Boolean,
    default: true
  },
  added: {
    type: Date,
    default: Date.now
  },
  meta: {
    last_query: Date,
    query_data: {}
  }
});



module.exports = mongoose.model('GameServer', GameServer);
