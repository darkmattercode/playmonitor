const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema

var VoiceServer = new Schema({
  type: String,
  ip: String,
  port: Number,
  qport: Number,
  active: {
    type: Boolean,
    default: true
  },
  added: {
    type: Date,
    default: Date.now
  },
  meta: {
    last_query: Date,
    query_data: {}
  }
});


module.exports = mongoose.model('VoiceServer', VoiceServer);
