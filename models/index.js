'use strict';
module.exports.user = require('./User');
module.exports.gameserver = require('./GameServer');
module.exports.voiceserver = require('./VoiceServer');
module.exports.games = require('./Games');
