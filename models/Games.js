const
  mongoose = require('mongoose'),
  Schema = mongoose.Schema

var Games = new Schema({
  slug: String,
  name: String,
  protocol: String,
  options: {},
  params: {}
});


module.exports = mongoose.model('Games', Games);
