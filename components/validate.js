'use strict';

const
  async = require('async'),
  models = require('./../models'),
  GameServer = models.game,
  Games = models.games

class Validate {
  static newServer(data) {
    if(!data.ip || !data.port || !data.slug){
      return false;
    }
    else {
      return true;
    }
  }

  static checkExists(model, data, callback){
    model.findOne(data, (err, result) => {
      if(err || !result){
        return callback(false);
      }
      callback(true);
    });
  }
}

module.exports = Validate;
