'use strict';

const
  Data = require('./data'),
  Model = require('./../models'),
  Status = require('./status');

class Tasks {
  static globalServerCheck() {
    Data.readAll(Model.gameserver, {}, (r) => {
      for(var i=0; i < r.length; i++) {
        Status.game(r[i].game, r[i].ip, r[i].port, r[i].qport, r[i], (status, pass) => {
          Data.update(Model.gameserver, {'_id':pass._id}, {'meta.last_query': Date.now(), 'meta.query_data': status}, (result) => {

          });
        });
      }
    });
    Data.readAll(Model.voiceserver, {}, (r) => {
      for(var i=0; i < r.length; i++) {
        Status.voice(r[i].type, r[i].ip, r[i].port, r[i].qport, r[i], (status,pass) => {

          Data.update(Model.voiceserver, {'_id':pass._id}, {'meta.last_query': Date.now(), 'meta.query_data': status}, (result) =>{

          });
        });
      }
    });
  }
}

module.exports = Tasks;
