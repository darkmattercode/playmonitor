'use strict';

const
  models = require('./../models'),
  GameServer = models.game,
  Games = models.games

class Data {
  static create(model, data, callback) {
    model.create(new model(data, (err, result) => {
      if(err){
        callback(err);
      }
      if(result) {
        callback(result);
      }
    }))
  }

  static read(model, data, callback) {
    model.findOne(data, (err, result) => {
      if(err || !result) {
        callback('No results' + 'ERROR: ' + err);
      }
      if(result){
        callback(result);
      }
    })
  }

  static readAll(model, data, callback) {
    model.find(data, (err, result) => {
      if(err || !result){
        callback('No Results' + 'ERROR: ' + err);
      }
      if(result){
        callback(result);
      }
    })
  }

  static update(model, conditions, data, callback) {
    model.update(conditions, data, (err, result) => {
      if(err){
        console.log(data + ' : ' + err)
        callback(err);
      }
      if(result){
        callback(result);
      }
    });
  }

  static delete(model, data, callback) {
    model.findOne(data, (err, result) => {
      if(err || !result) {
        callback('No Result' + 'ERROR: ' + err);
      }
    }).remove().exec();
  }


}

module.exports = Data;
