'use strict';

const
  models = require('./../models'),
  query = require('game-server-query'),
  dot = require('dot-object')

class Status {

  static game(slug, ip, port, qport, pass, callback) {
    var q = {
      type: slug,
      host: ip,
      port: port
    };
    if(qport != null){
      q.port_query = qport;
    }
    query(q,
    function(state) {
      if(state.error){
        console.log(state);
        callback(state, pass);
      }
      else {
        if(state.query.type == 'ts3'){
          var count = 0;
          var players = state.players;
          for(var i=0; i < players.length; i++){
            if(players[i].client_type == '0'){
              count += 1;
            }
          }
          state.online = count;
        }
        Status.removeDots(state, (err, result) => {
          //console.log(result);
          callback(result, pass);
        });
      //console.log(state);
      }
    })
  }

  static removeDots(obj, callback) {
    for(var k in obj.raw.rules) {
      if(k.replace('.', '_') != k) {

        obj.raw.rules[k.replace('.', '_')] = obj.raw.rules[k];
        delete obj.raw.rules[k];
      }

    }
//    console.log(obj);
    callback(null, obj);
  }

  static voice(slug, ip, port, qport, pass, callback) {
    var q = {
      type: slug,
      host: ip,
      port: port,
      port_query: qport
    };
    query(q,
    function(state) {
      if(state.error) {
        callback(state, pass);
      }
      else {
        callback(state, pass);
      }
    })
  }

}

module.exports = Status;
