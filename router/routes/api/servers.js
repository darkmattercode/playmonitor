const
  express = require('express'),
  router = express.Router(),
  Data = require('./../../../components/data'),
  Validate = require('./../../../components/validate'),
  Model = require('./../../../models')

router.post('/game', function(req, res, next){

  var data = {
    'ip' : req.body.ip,
    'port': req.body.port,
    'slug' : req.body.slug,
    'qport': req.body.qport || null
  }
  console.log(data);
  var valid = Validate.newServer(data);
  if(valid){
    Validate.checkExists(Model.games, {'slug': data.slug}, (exists) => {
      if(exists) {
        Validate.checkExists(Model.gameserver, {'ip': data.ip, 'port': data.port}, (exists) => {
          if(!exists) {
            Data.create(Model.gameserver, {'ip': data.ip, 'port': data.port, 'game': data.slug, 'qport': data.qport});
            
          }
          if(exists) {
            console.log('exists');
          }
        })
      }
    });
  }
});

router.post('/voice', function(req, res) {
  var data = {
    'ip' : req.body.ip,
    'port' : req.body.port,
    'slug' : req.body.slug,
    'qport' : req.body.qport,
    'title' : req.body.title
  }
  var valid = Validate.newServer(data);
  console.log(valid);
  if(valid){
    console.log('test');
    Validate.checkExists(Model.voiceserver, {'ip': data.ip, 'port':data.port}, (exists) => {
      if(!exists) {
        Data.create(Model.voiceserver, {'ip':data.ip, 'port':data.port,'type':data.slug, 'qport': data.qport, 'title': data.title})
      }
      if(exists) {
        console.log('exists');
      }
    })
  }
})


module.exports = router;
