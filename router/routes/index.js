const
  express = require('express');
  router = express.Router(),
  passport = require('passport');

var models = require('./../../models');
var user = models.user;

router.get('/', function(req, res){
  if(req.user){
    res.redirect('/dashboard');
  }
  res.render('index', {user: req.user})
});

router.get('/secrettunnel', function(req,res) {
  res.render('register', {});
});

router.post('/secrettunnel', function(req, res) {
  user.register(new user({username: req.body.username}),req.body.password, function(err,account) {
    if(err) {
      return res.render('register', {account: account});
    }

    passport.authenticate('local')(req,res, function(){
      res.redirect('/');
    });
  });
});

router.get('/login', function(req, res) {
  res.render('login', {user:req.user});
});

router.post('/login', passport.authenticate('local'), function(req, res){
  res.redirect('/')
});

router.get('/logout', function(req, res){
  req.logout();
  res.redirect('/');
});




module.exports = router;
