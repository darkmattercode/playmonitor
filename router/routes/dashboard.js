const
  express = require('express'),
  router = express.Router(),
  Data = require('./../../components/data'),
  Model = require('./../../models'),
  Status = require('./../../components/status')

router.get('*', function(req, res, next) {
  /*if(!req.user){
    res.redirect('/');
  }
  res.locals.user = req.user;
  */next();
});

router.get('/', function(req, res){
  res.render('dashboard');
});

router.get('/addserver', function(req, res) {
  //data
  res.render('addserver');
});

router.get('/gameservers', function(req, res) {
  var servers = [];
  Data.readAll(Model.gameserver, {}, (r) => {
    if(r){
      res.render('gameservers', {servers: r});
    }
  });
});

router.get('/gameservers/:id', function(req, res) {
  Data.read(Model.gameserver, {'_id': req.params.id}, (r) =>{
    console.log(r.meta.query_data);
    res.render('./games/'+ r.game + '.jade', {server: r.meta});
  });
});

router.get('/voiceservers', function(req, res) {

});

router.get('/voiceservers/:id', function(req, res) {
  Data.read(Model.voiceserver, {'_id': req.params.id}, (r) =>{
    res.render('voiceserver', {server: r});
  })
});

module.exports = router;
