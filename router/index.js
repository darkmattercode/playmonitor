module.exports = function (app) {
  app.use('/', require('./routes/index'));
  app.use('/dashboard', require('./routes/dashboard'));
  app.use('/api/servers', require('./routes/api/servers'));
}
