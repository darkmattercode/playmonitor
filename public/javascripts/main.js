$(document).ready(function() {

  loadGames();

  function loadGames() {
    var url, option;
    url = '../resources/games.json';

    $.getJSON(url, function(data){
      $(data).each(function() {
        option = '<option value=' + this.slug + '>' + this.name + '</option>';
        $('#add-games-list').append(option);
      })
    })
  }

  // CLICK DETECTIONS

  // CLICK ON GAMESERVER ROW IN TABLE
  $(document).on('click', 'tr[id^="GS-"]', function(){
    console.log('click');
    var id = this.id.replace('GS-', '');
    window.location.href='/dashboard/gameservers/' + id;
  });

  // MODALS

  // ADD GAMESERVER MODAL
  $('#addserverbtn').on('click', function() {
    $('#addserver').modal('show');
  });

});
